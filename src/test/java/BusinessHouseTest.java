import BusinessHouse.Board;
import BusinessHouse.BusinessHouse;
import Dice.Dice;
import Player.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class BusinessHouseTest {
    @Test
    void diceShouldBe30TimesInTheGame() {
        Player player = new Player(1000);
        Player otherPlayer = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        players.add(otherPlayer);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(2);

        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();
        verify(mockDice, times(20)).roll();
    }

    @Test
    void playerOneShouldBe10ThPosition() {
        Player player = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(1);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(10, player.move(0));
    }
    @Test
    void playerPositionShouldBeWithInBoard() {
        Player player = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(5);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(5, player.move(0));
    }

    @Test
    void playerShouldHave1HotelsIfDiceOutComeIs1() {
        Player player = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(1);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(1, player.totalHotel());
    }

    @Test
    void playerShouldHave3HotelsIfDiceOutComeIs4() {
        Player player = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(4);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(3, player.totalHotel());
    }

    @Test
    void positionTwoShouldBe10ThPosition() {
        Player player = new Player(1000);
        Player otherPlayer = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        players.add(otherPlayer);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(2);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(20, otherPlayer.move(0));
    }

    @Test
    void playerThreeShouldBeAt30ThPosition() {
        Player player = new Player(1000);
        Player otherPlayer = new Player(1000);
        Player someOtherPlayer = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        players.add(otherPlayer);
        players.add(someOtherPlayer);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(3);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(30, someOtherPlayer.move(0));
    }

    @Test
    @DisplayName("playerOneShouldHaveBalance550IfDiceOutcomeIs1EveryTime")
    void shouldHaveBalance900() {
        Player player = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(1);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(900, player.balance());
    }

    @Test
    @DisplayName("playerTwoShouldHaveBalance1100IfDiceOutcomeIs1EveryTime")
    void playerTwoShouldHaveBalance1050() {
        Player player = new Player(1000);
        Player otherPlayer = new Player(1000);
        List<Player> players = new ArrayList<>();
        players.add(player);
        players.add(otherPlayer);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(1);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(1050, otherPlayer.balance());
    }

    @Test
    @DisplayName("playerThreeShouldHaveBalance1300IfDiceOutcomeIs1EveryTime")
    void playerThreeShouldHaveBalance1050() {
        Player player = new Player(1000);
        Player otherPlayer = new Player(1000);
        Player someOtherPlayer = new Player(1000);

        List<Player> players = new ArrayList<>();
        players.add(player);
        players.add(otherPlayer);
        players.add(someOtherPlayer);
        Dice mockDice = mock(Dice.class);
        when(mockDice.roll()).thenReturn(1);
        Board board = new Board();
        BusinessHouse businessHouse = new BusinessHouse(players, board, mockDice);

        businessHouse.startGame();

        assertEquals(1050, someOtherPlayer.balance());
    }

}
