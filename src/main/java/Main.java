import BusinessHouse.*;
import Dice.Dice;
import Player.Player;

import java.util.*;

public class Main {

    public static void main(String... arg) {
        Dice dice = new Dice(12);
        Board board = new Board();

        Player player = new Player(1000);
        Player otherPlayer = new Player(1000);
        Player someOtherPlayer = new Player(1000);
        List<Player> listOfPlayers =
                Arrays.asList(
                        player,
                        otherPlayer,
                        someOtherPlayer);

        BusinessHouse businessHouse = new BusinessHouse(listOfPlayers, board, dice);

        businessHouse.startGame();
    }
}
