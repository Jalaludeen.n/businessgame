package BusinessHouse;

import Cells.Cell;
import Dice.*;
import Player.*;

import java.util.ArrayList;
import java.util.List;

public class BusinessHouse {
    public static final int CHANCE_PER_PLAYER = 10;
    private Dice dice;
    private Board board;
    private int nextTurn;
    private final int totalChances;
    private int remainingChances;
    private final List<Player> players;

    public BusinessHouse(List<Player> players, Board board, Dice dice) {
        this.players = new ArrayList<>();
        this.players.addAll(players);
        this.board = board;
        this.dice = dice;
        nextTurn = 0;
        totalChances=players.size() * CHANCE_PER_PLAYER;
        remainingChances=0;
    }

   public void startGame() {
        int diceOutCome;
        int position;
        Player currentPlayer;
        while (remainingChances<totalChances) {
            diceOutCome = dice.roll();
            currentPlayer = currentPlayer();
            position = currentPlayer.move(diceOutCome);

            Cell currentCell = board.getCell(position);
            currentCell.updateAmount(currentPlayer);
            remainingChances++;
        }
        printTotalWorth();
    }

    public void printTotalWorth() {
        for (Player player : players) {
            player.printTotalAmount();
        }
    }

    private Player currentPlayer() {
        int totalPlayers = players.size();
        int currentPlayer = nextTurn % totalPlayers;
        nextTurn++;
        return players.get(currentPlayer);
    }


}