package BusinessHouse;
import Cells.*;
import java.util.HashMap;

public class Board {
    private static final int ONE = 1;
    final private HashMap<Integer, Cell> cellPositions;

    public Board() {
        cellPositions = initializeCellPositions();
    }

    private HashMap<Integer, Cell> initializeCellPositions() {
        final HashMap<Integer, Cell> cellPositions = new HashMap<>();
        final String positions = "EEJHETJTEEHJTHEEJHETJTEEHJTHJEEJHETJTEEHJTEHE";

        for (int index = 0; index < positions.length(); index++) {
            char type = positions.charAt(index);
            if (type == 'E') {
                cellPositions.put(index + ONE, new Empty());
            }
            if (type == 'H') {
                cellPositions.put(index + ONE, new Hotel());
            }
            if (type == 'J') {
                cellPositions.put(index + ONE, new Jail());
            }
            if (type == 'T') {
                cellPositions.put(index + ONE, new Treasure());
            }

        }
        return cellPositions;
    }

    public Cell getCell(int position) {
         return cellPositions.get(position);
    }
}
