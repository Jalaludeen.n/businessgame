package Player;

import Cells.Hotel;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private int amount;
    private int currentPosition;
    private List<Hotel> ownedHotels = new ArrayList<>();
    //TODO testing purpose
    public int balance() {
        return amount;
    }

    public Player(int amount) {
        this.amount = amount;
    }

    public int move(int ahead) {
        currentPosition += ahead;
        return controlPositionOnBoard();
    }

    private int controlPositionOnBoard() {
        if (currentPosition > 45) {
            return currentPosition - 45;
        }
        return currentPosition;
    }

    public void receiveHotelRent(int rent) {
        amount += rent;
    }

    public void payRent(int rent) {
        amount -= rent;
    }

    public void buyHotel(int hotelPrize, Hotel hotel) {
        amount -= hotelPrize;
        ownedHotels.add(hotel);
    }

    public void payFineForJail(int fine) {
        amount -= fine;
    }

    public void getsTreasure(int treasure) {
        amount += treasure;
    }

    //TODO testing purpose
    public int totalHotel()
    {
        return ownedHotels.size();
    }

    public void printTotalAmount() {
        int totalHotels = ownedHotels.size() * 200;
        System.out.println(amount + totalHotels);
    }
}
