package Cells;

import Player.Player;

public class Hotel implements Cell {
    private Player owner;
    private static final int HOTEL_PRIZE = 200;
    private static final int RENT = 50;

    @Override
    public void updateAmount(Player player) {
        if (owner == null) {
            owner = player;
            player.buyHotel(HOTEL_PRIZE, this);
        } else {
            player.payRent(RENT);
            owner.receiveHotelRent(RENT);
        }

    }

}
