package Cells;
import Player.Player;

public class Treasure implements Cell {
    private static final int TREASURE = 200;

    @Override
    public void updateAmount(Player player) {
        player.getsTreasure(TREASURE);
    }
}
