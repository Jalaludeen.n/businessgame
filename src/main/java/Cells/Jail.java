package Cells;
import Player.Player;

public class Jail implements Cell {
    private static final int FINE = 150;

    @Override
    public void updateAmount(Player player) {
        player.payFineForJail(FINE);
    }

}
