package Cells;
import Player.Player;

public interface Cell {
    void updateAmount(Player player);
}
